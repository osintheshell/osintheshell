<?xml version='1.0'?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0">
    <xsl:import href="/usr/share/publican/xsl/pdf.xsl"/>
	<xsl:template match="title" mode="chapter.titlepage.recto.auto.mode">  
	<fo:block xmlns:fo="http://www.w3.org/1999/XSL/Format" 
            xsl:use-attribute-sets="chapter.titlepage.recto.style" 
            margin-left="{$title.margin.left}" 
            font-size="14pt" 
            font-weight="bold" 
            font-family="{$title.font.family}">
    	<xsl:call-template name="component.title">
      <xsl:with-param name="node" select="ancestor-or-self::chapter[1]"/>
    </xsl:call-template>
  </fo:block>
</xsl:template>

<xsl:template match="processing-instruction('pagebreak')">
</xsl:template>
</xsl:stylesheet>

