# Makefile

book: *.xml credits
	@echo '<!DOCTYPE book PUBLIC "-//OASIS//DTD Docbook XML V4.4//EN"' > docbook.header
	@echo '"'`find /usr/ -type f -iname "*docbookx*dtd" 2> /dev/null | grep -i xml`'">' >> docbook.header
	@cat docbook.header \
		00.xml \
		preface.xml \
		desktop.xml \
		web.xml \
		text.xml \
		email.xml \
		emacs.xml \
		printing.xml \
		backup.xml \
		software.xml \
		credits | iconv -f ISO-8859-1 -t UTF-8 > tmp.xml
	echo "Now type make html|text|pdf"

html: docbook.header *.xml credits
	xmlto html tmp.xml -o ./html

text: docbook.header *.xml credits
	xmlto txt tmp.xml -o ./txt

pdf: docbook.header *.xml credits
	xmlto fo tmp.xml -o pdf
	fop pdf/tmp.fo pdf/commandLineOS.pdf

tidy:
	-rm -f ./pdf/*.fo tmp.xml

clean:
	-rm -f *.header tmp.xml
	-rm -rf html
	-rm -f ./pdf/*.pdf
	-rm -f ./pdf/*.fo
